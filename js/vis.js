// This will be a force-directed graph (https://bl.ocks.org/mbostock/4062045 or
// https://bl.ocks.org/mbostock/950642 and http://bl.ocks.org/jhb/5955887 for
// the labeled variety) which shows links over time.
// So basically the animation part is that it goes through time and updates the
// links as time passes
// See https://bl.ocks.org/mbostock/1095795 and
// https://bl.ocks.org/mbostock/0adcc447925ffae87975a3a81628a196 also for
// dynamic node/link modification
//
// TIPS:
// don't use the d3.forceCenter force, use d3.forceX and d3.forceY, since
// d3.forceCenter makes it so that nodes aren't continuously moving
//
// set alphaTarget to 0.3 or something so that the graph is continuously moving
//
// you can set alphaTarget to 0.3 every time the vis ticks time-wise
//
// use a slider that gets moved every tick while playing to control time
//
// TODOS:
// TODO: End time? (add end field to data.json and check if date state is
// greater than time field and less than end field, if end field exists)
//
// TODO: Node start/end time? (this is where the animated links come in)
//       For instance if someone met another person through a proxy but then
//       they forgot about the proxy friend
//
// TODO: Add strength field to data.json? ("Friendship strength")

// ---------------
// -- CONSTANTS --
// ---------------

const dataLoc = "../data.json";
const colors = {
  line: {
    default: "#999999",
    new: "#f4ac41",
    hover: "#8bef9d",
    nodeHover: "#42cef4"
  },
  circle: {
    default: "#ffffff",
    hover: "#000000",
    lineHover: "#000000"
  }
}
const wrapWidth = 250;

// ---------------
// -- FUNCTIONS --
// ---------------

function update(nd, ld) {
  // Apply the general update pattern to the links.
  link = link.data(ld, d => d.source.id + "-" + d.target.id);

  y = link.enter().append("g")
      .attr("class", "links");

  // This fake line lets us artificially increase the mouseover area
  y.append("line")
      .attr("class", "fake")
      .style("stroke-opacity", 0)
      .style("stroke-width", 20);

  y.append("line")
      .attr("class", "real")
      .style("stroke", colors.line.new)
      .style("stroke-width", 2)
      .transition()
        .duration(650)
        .style("stroke-opacity", 1)
        .style("stroke-width", 1)
        .style("stroke", d => colors.line.default);

  link.exit()
    .transition().select("line.real")
      .style("stroke-width", 0)
      .attrTween("x1", d => () => d.source.x)
      .attrTween("x2", d => () => d.target.x)
      .attrTween("y1", d => () => d.source.y)
      .attrTween("y2", d => () => d.target.y);

  link.exit().transition().remove();

  link = y.merge(link);

  link.on("mouseenter", d => {
    d3.select(this).transition().select(".real").duration(50).style("stroke-width", 5).style("stroke", colors.line.hover);
    node.transition().select("circle").style("stroke", n => (d.source === n || d.target === n) ? colors.circle.lineHover : colors.circle.default);
    infoT.text(d.meta);
    svg.selectAll(".infoT").call(wrap, wrapWidth);
  });

  link.on("mouseleave", d => {
    d3.select(this).transition().select(".real").duration(50).style("stroke-width", 1).style("stroke", colors.line.default);
    node.transition().select("circle").style("stroke", colors.circle.default);
    infoT.text("");
  });

  // Apply the general update pattern to the nodes.
  node = node.data(nd, d => d.id);

  // Note, in order to transition a child element of the g, first do node.exit().transition().select(whatever),
  // then do node.exit().transition().remove() (the 2nd .transition() stops the elements from immediately
  // being removed)
  // see https://stackoverflow.com/questions/14864262/how-to-exit-transition-rects-under-a-col-in-d3

  // This shouldn't happen
  node.exit().remove();

  x = node.enter().append("g")
      .attr("class", "nodes");

  x.append("circle")
      .attr("fill", d => color(d.id))
      .attr("x", -8)
      .attr("y", -8)
      .transition()
        .attr("r", 12);

  x.append("text")
      .attr("x", 4)
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(d => d.id);

  node = x.merge(node);

  node.on("mouseover", function(d) {
    link.transition().selectAll("line.real")
        .duration(175)
        .style("stroke-width", l => (d === l.source || d === l.target) ? 3 : 1)
        .style("stroke", l => (d === l.source || d === l.target) ? colors.line.nodeHover : colors.line.default);
    d3.select(this).select("circle").transition().style("stroke", colors.circle.hover);
    infoT.text(d.id + ": " + link.data().filter(l => d === l.source || d === l.target).length + " associations");
    svg.selectAll(".infoT").call(wrap, wrapWidth);
  });

  node.on("mouseout", function(d) {
    link.transition().selectAll("line.real")
        .duration(175)
        .style("stroke-width", 1)
        .style("stroke", colors.line.default);
    d3.select(this).select("circle").transition().style("stroke", colors.circle.default);
    infoT.text("");
  });

  // Update and restart the simulation.
  simulation.nodes(nd);
  simulation.force("link").links(ld);
  simulation.alpha(0.6).restart();
}

function tick() {
  node.attr("transform", d => "translate(" + d.x + "," + d.y + ")");

  link.selectAll("line")
      .attr("x1", d => d.source.x)
      .attr("y1", d => d.source.y)
      .attr("x2", d => d.target.x)
      .attr("y2", d => d.target.y);

  // Move progress bar playhead if playing
  // Time stuff
  timeT.text(fmtTime(interpTime(progress)));
  update(nodes, links.filter(v => prsTime(v.time) <= interpTime(progress)));
}

function inter() {
  if (!click) {
    var v = +svg.select("#progress").property("value");
    var e = new CustomEvent("manual", { "detail": v + pps / ups })
    svg.select("#progress").node().dispatchEvent(e);
  }

  if (start) setTimeout(inter, 1000 / ups);
}

function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        x = text.attr("x"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

// ------------------------
// -- VARIABLES AND SUCH --
// ------------------------

var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height"),
    link = svg.append("g").selectAll(".links"),
    node = svg.append("g").selectAll(".nodes");

var color   = d3.scaleOrdinal(d3.schemeCategory10),
    prsTime = d3.timeParse("%m-%Y"),
    fmtTime = d3.timeFormat("%B %Y");

var progress = 0,
    interpTime = d3.interpolate(new Date(2013, 7, 1), new Date);

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(d => d.id).distance(150).strength(0.35))
    .force("charge", d3.forceManyBody().strength(-1000))
    .force("cx", d3.forceX().x(width / 2).strength(0.07))
    .force("cy", d3.forceY().y(height / 2).strength(0.07))
    .on("tick", tick)
    .stop();

var timeT = svg.append("text")
    .attr("x", 50)
    .attr("y", height - 175)
    .attr("opacity", 0.5)
    .attr("font", "Iosevka")
    .attr("font-size", "32")
    .text(fmtTime(interpTime(progress)));

var infoI = svg.append("text")
    .attr("x", 50)
    .attr("y", 55)
    .attr("font-size", "18px")
    .text("Background info:");

var infoT = svg.append("text")
    .attr("class", "infoT")
    .attr("x", 50)
    .attr("y", 90)
    .attr("dy", "0px")
    .attr("font-size", "24px");


var ups = 60,
    pps = 7.5;

// ------------------
// -- GLOBAL STATE --
// ------------------

var nodes = [],
    links = [];

var click = false;
var start = false;
var interval;

// --------------------
// -- POST-DATA LOAD --
// --------------------

d3.json(dataLoc, (error, json) => {
  if (error) throw error

  links = json.links;
  nodes = json.nodes;

  var input = svg.select("#progress")
    .property("value", 0)
    .on("input", (_, i, ns) => {
      click = true;
      progress = (ns[i].value > 100 ? 100 : ns[i].value) / 100;
    })
    .on("manual", (_, i, ns) => {
      var v = d3.event.detail;
      if (v > 100) {
        progress = 1;
        // This is so that if we hit the end while playing we stop
        if (start) startB.node().dispatchEvent(new Event("click"));
      } else {
        progress = v / 100;
      }
      ns[i].value = progress * 100;
    })
    .on("mouseup", () => {
      click = false;
    })

  var speedT = svg.select("#speed-num")
    .text(pps);

  var speed = svg.select("#speed")
    .property("value", pps)
    .on("input", (_, i, ns) => {
      speedT.text(ns[i].value);
      pps = ns[i].value;
    });

  var startB = svg.select("#start")
    .on("click", () => {
      if (start) {
        start = false;
        startB.text("Start");
      } else {
        start = true;
        startB.text("Pause")
        inter();
        speed.node().dispatchEvent(new Event("input"));
      }
    });

  simulation.restart();
});